﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Codechangers.Startup))]
namespace Codechangers
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
